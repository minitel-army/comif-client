import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultLayoutComponent} from './layouts/default-layout/default-layout.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {DashboardPageComponent} from './pages/dashboard-page/dashboard-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DefaultLayoutComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    DashboardPageComponent
  ]
})
export class AdminModule {
}

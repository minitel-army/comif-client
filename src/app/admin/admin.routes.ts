import {Routes} from '@angular/router';
import {DashboardPageComponent} from './pages/dashboard-page/dashboard-page.component';

export const adminRoutes: Routes = [
  {
    path: 'admin', children: [
      {
        path: '', component: DashboardPageComponent
      }
    ]
  }
];

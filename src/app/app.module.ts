import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {adminRoutes} from './admin/admin.routes';
import {publicRoutes} from './public/public.routes';
import {AdminModule} from './admin/admin.module';
import {PublicModule} from './public/public.module';
import {SharedModule} from './shared/common.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([...adminRoutes, ...publicRoutes]),
    HttpClientModule,
    AdminModule,
    PublicModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

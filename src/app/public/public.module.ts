import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {DefaultLayoutComponent} from './layouts/default-layout/default-layout.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HomePageComponent, DefaultLayoutComponent]
})
export class PublicModule {
}

import {Routes} from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';

export const publicRoutes: Routes = [
  {
    path: '', component: HomePageComponent
  }
];
